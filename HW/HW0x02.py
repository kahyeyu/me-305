'''@file HW0x02.py
@brief Simulates the movement of an elevator
@details Uses a finite state machine to simulate the movement of the elevator
 and the py.file can be found at https://bitbucket.org/kahyeyu/kahyeyu.bitbucket.io/src/master/HW0x02.py
@author Kahye Yu
@date 1/26/21
'''

import time
import random

# functions go here
def motor_cmd(cmd):
    ''' @brief Outputs the behavior of the motor
        @details Given some input comamand, the function will print the movement
        of the elevator
        @param cmd the input of the desired movement for the elevator
        @return A printed output of what the elevator is doing
    '''
    
    if cmd=='Hold':
        print('Arrived')
    elif cmd=='Up':
        print('Button 2 on')
        print('Moving Up')
    elif cmd=='Down':
        print('Button 1 on')
        print('Moving Down')
        
def floor_1():
     ''' @brief Outputs random true or false for floor_1 behavior
    @param True or False
    '''
     return random.choice([True, False])

def floor_2():
     ''' @brief Outputs random true or false for floor_2 behavior
    @param True or False
    '''
     return random.choice([True, False])

def button_1(a):
     ''' @brief Outputs random true or false for button_1 behavior
    @param True or False
    '''
     if a==None:
         return random.choice([True, False])
         # if button 1 is unassigned, will randomly output true or false
     else:
         print('Button 1 off')
         # if button 1 is set to be 0 (aka off), code prints this behavior

def button_2(a):
     ''' @brief Outputs random true or false for button_2 behavior
    @param True or False
    '''
     if a==None:
        return random.choice([True, False])
        # if button 2 is unassigned, will randomly output true or false
     else: 
        print('Button 2 off') 
        # if button 2 is set to be 0 (aka off), code prints this behavior

# Main program
if __name__=="__main__":
    # initial state
    state = 0
    
    while True:
        a=None
        # clearing buttons for every pass
        try:
            if state == 0:
                print('S0')         # moving down
                if floor_1():
                    motor_cmd('Hold')
                    button_1(0)
                    state=1
                    print('S1')
                   
                
            elif state == 1:
                print('S1')         # stopped on floor 1
                if button_1(a):
                    button_1(0)
                elif button_2(a):
                    motor_cmd('Up')
                    state=2
                    print('S2')
                
            elif state == 2:
                print('S2')         # moving up
                if floor_2():
                    motor_cmd('Hold')
                    button_2(0)
                    state=3
                    print('S3')
                    
            elif state == 3:
                print('S3')         # stopped on floor 2
                if button_2(a):
                    button_2(0)
                elif button_1(a):
                    motor_cmd('Down')
                    state =0
                    print('S0')
                    
            else:
                pass
            
            time.sleep(2)
            
        # except KeyboardInterrupt():
        #     break
        
    # program deinitialization    
    print('Done')      
            

