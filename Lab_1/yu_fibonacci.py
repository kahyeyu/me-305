''' @file yu_fibonacci.py
    @author Kahye Yu
    @date 1/20/2021     
'''

f = [-1]*1001 

def fib(idx):
    '''@brief Computes fibonacci number at idx up to 1000
        @detail This function accepts input index, idx, and computes the
        fibonacci number using the a bottom up approach. Initially, a place 
        holder array is defined and as fibonacci values are computed they are 
        stored in the array. These tabulated values are then called when 
        needed for computing fibonacci numbers for greater index values.
        @param idx, desired index for which to find the fibonacci number.
        @return fib(idx), the fibonacci number at the specified index
    '''
        
    if (f[idx] < 0):
        if (idx==0):
            f[idx] = 0
        elif (idx == 1):
            f[idx] = 1
        else:
            f[idx] = fib(idx-1) + fib(idx-2)
    return f[idx]
        
if __name__ == '__main__':
    while True:
        try:
            string=input('Please insert index value idx: ')
            idx=int(string)
        
            print ('Fibonacci number at '
            'index {:} is {:}.'.format(idx,fib(idx)))
        except KeyboardInterrupt:
                break 
print('Thank you for participating')
