'''@file FF_1_UI_data_gen.py
@brief This code waits for an input from the user over the PC and collects and sends data once given the character 'g'.
@author Kahye Yu
@date 4/4/21
@details The data is collected and held in an array where the code will encode then send over single lines of data via serial commnication.
The FSM is shown here
@image FF_1_FSM.jpg "Finite State Machine for UI_data_gen Week 1" width=600cm

'''


from math import sin, exp, pi
from pyb import USB_VCP
from array import array

myusbvcp = USB_VCP()

class UI_data_gen:

    def __init__(self, g=True):
        self.g = g
        self.state = 0
        
        self.times  = array('f', list(self.time/10 for self.time in range(0,301)))
        self.values = array('f', 301*[0])
        self.data_lines = array('f', 301*[0])  
        self.n = 0  
        
        
    def run(self):
        
        if self.state == 0: # waiting state
        
            if myusbvcp.any():
                self.val = myusbvcp.readline().decode() 
            
                if self.val == 'g':
                    self.state = 1
                
        elif self.state == 1:
            self.values[self.n] = exp(-self.times[self.n]/10)*sin(2*pi/3*self.times[self.n])
            self.n += 1
            
            if self.n >= len(self.times):
                self.n = 0
                self.state = 2
                    
        
        elif self.state == 2: # Data sending state
            self.data_lines = '{:}, {:}\r\n'.format(self.times[self.n], self.values[self.n])
            self.n += 1
            myusbvcp.write(self.data_lines.encode())

                    
            if self.n >= len(self.times):
                pass
                
                    
               
            
            
            
            
            
            # self.i = 0
            # for self.i in range(len(self.data_lines)):
            #     self.send_encoded_line_of_data = self.data_lines[self.i].encode
            #     self.i += 1
                


        


