'''@file FF_1_UI_front.py
@brief This code is the user interface portion for the first week of the term project.
@author Kahye Yu
@date 4/4/21
@detail The user inputs the character 'g', which gets encoded and sent over serial as the input to start collecting data on the nucleo.
The source code is found on my bitbucket account here:


'''


import serial
from matplotlib import pyplot
from array import array
  
def sendChar():
    '''@brief This function send over the character 'g' over serial communication in which it will be recieved by the nucleo to prompt it to start collecting data. 
    @detail Once the data is collected on the nucleo, it will be sent back one by one and fill the array or zeros for time and values. 
    @param g Which is input in by the user from the main snippets of code.
    @return time and values of 30 seconds of data collectino along with a plot.
    '''
    i = 0
    times = array('f', 301*[0])
    values = array('f', 301*[0])
    
    # while True:
    ser.write('g'.encode())
    
    while True:
        lineString = ser.readline().decode()
        print(lineString)
        #Remove line endings
        StrippedString = lineString.strip()

        # split on the commas
        SplitStrings = StrippedString.split(',')
    
        # Convert entries to numbers from strings
        times[i] = float(SplitStrings[0])
        
        
        values[i] = float(SplitStrings[1])
        
        i += 1
        
        if i >= 301:
            pyplot.figure()
            pyplot.plot(times, values)
            pyplot.xlabel('Time')
            pyplot.ylabel('Data')
            break

with serial.Serial(port='/dev/tty.usbmodem2064337757522',baudrate=115273,timeout=1) as ser: 

    while True:
        user_input = input('Give me a character: ')
        if user_input == 'g':
            char = sendChar()
            print('Data Collection and plot complete\nThe plot if found in the Plots tab\nThank you for participating!')
            break
        
        
        elif user_input == 's':
            print('Thanks for participating!')
            break
    
        else:
            print('not g')
    
        
    
    