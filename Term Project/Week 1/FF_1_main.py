'''@file main.py
@brief This code runs the data generation class where it waits for the user input.
@author Kahye Yu
@date 4/4/21
@detail It is the default code that runs when the nucleo is powered.
'''



from pyb import USB_VCP
from UI_data_gen import UI_data_gen

myusbvcp = USB_VCP()

if __name__=="__main__":
    data_collection_task = UI_data_gen(True)
    
    while True:
        try:
            data_collection_task.run()
        
        except KeyboardInterrupt:
            print('Ctrl-c has been pressed')
            break
            
            