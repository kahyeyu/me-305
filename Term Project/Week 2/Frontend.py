''' @Frontend.py
    @brief This code is the interface seen on the PC by the user via the console window in Spyder. The user inputs variables which trigger certain functions from the Encoder
    @author Kahye Y
    @date 4/4/21
    @details The four possible variables are z, p, d and s. If the user inputs z, the encoder count will zero. If the user inputs p, the position of the encoder will be output. 
    If the user inputs d, the difference between encoder counts will output. If the user inputs s, the code will stop running. Once run, this code is responsible for the running the rest of the task diagram. 
    The task diagram is seen below.
    @image html task_diagram.jpg "Task Diagram for Week 2 of Term Project" width=600cm
    The source code is found on my bitbucket account:

'''

import serial
  
def send_z():
    '''@brief This function sends the character 'z' over the serial communication.
    @details The 'z' is encoded to be sent over serial, it is then recieved by the nucleo and decoded. Once the character is recieved, the nucleo will zero the encoder and let the user know it has done so.
    @param z which is input by the user from the main code
    @return myval is returned, which is a message that the encoder has been been set to zero.
    '''
    ser.write('z'.encode())
    myval = ser.readline().decode()
    return myval

def send_p():
    '''@brief This function sends the character 'p' over the serial communication.
    @details The 'p' is encoded to be sent over serial, it is then recieved by the nucleo and decoded. Once the character is recieved, the nucleo will output the current position and let the user know it has done so.
    @param p which is input by the user from the main code
    @return myval is returned, which is the position of the encoder in degrees.
    '''
    ser.write('p'.encode())
    myval = ser.readline().decode()
    return myval
    
def send_d():
    '''@brief This function sends the character 'd' over the serial communication.
    @details The 'd' is encoded to be sent over serial, it is then recieved by the nucleo and decoded. Once the character is recieved, the nucleo will output the count difference and let the user know it has done so.
    @param d which is input by the user from the main code
    @return myval is returned, which is the difference in the encoder count.
    '''
    ser.write('d'.encode())
    myval = ser.readline().decode()
    return myval
    
def send_s():
    '''@brief This function sends the character 's' over the serial communication.
     @details The 's' is encoded to be sent over serial, it is then recieved by the nucleo and decoded. Once the character is recieved, the code will end.
     @param s which is input by the user from the main code
     @return break which ends the code
     '''
    
    ser.write('s'.encode())
    myval = ser.readline().decode()
    return myval


with serial.Serial(port='/dev/tty.usbmodem2064337757522',baudrate=115273,timeout=1) as ser: 

    while True:
        user_input = input('Give me a character: ')
        if user_input == 'z':
            print('z pressed')
            char = send_z()
            print(char)
           
        elif user_input == 'p':
            print('p pressed')
            char = send_p()
            print(char)
        
        elif user_input == 'd':
            print('d pressed')
            char = send_d()
            print(char)
        
        elif user_input == 's':
            print('Thanks for participating!')
            break
    
        else:
            print('not g')
    
        
    
    