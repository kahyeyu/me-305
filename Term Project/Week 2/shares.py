'''@files shares.py
@brief This code is the holder of shared variables between the two tasks. 
@author Kahye Yu
@date 4/4/21
@details The source code is found here on my bitbucket account:
It is a bank of variables that come from the encoder task. The UI task is able to access this bank of variables to be sent over serial.
'''

from Encoder_task import Encoder_task

update = '{:}\r\n'.format(Encoder_task.update)
zero_position = '{:}\r\n'.format(Encoder_task.get_zero)
get_position = '{:}\r\n'.format(Encoder_task.get_position)
get_delta = '{:}\r\n'.format(Encoder_task.get_delta)














