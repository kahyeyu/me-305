'''@file This code is the driver for the encoder in which it is responsible for running various methods to get certain values from the motor.
@brief This code is a class that is used in the Encoder_task.py file and uses classes to update the count of the encoder, reset the encode count, output the current position of the encoder and output the difference between counts.
@author Kahye Yu
@date 4/4/21
@detail The source code is found here on my bitbucket account:


'''
import utime
import pyb
from pyb import USB_VCP
USB_VCP 

class Encoder_Driver:
    
    def __init__(self, run=True):
        self.run = run
    

        # Setting pins, timer and timer channels on board
        self.pinB6 = pyb.Pin(pyb.Pin.cpu.B6) 
        self.pinB7 = pyb.Pin(pyb.Pin.cpu.B7) 
        self.timer = pyb.Timer(4, prescaler = 0, period = 65535)
        self.t4ch1 = self.timer.channel(1, mode=pyb.Timer.ENC_AB, pin=self.pinB6)
        self.t4ch2 = self.timer.channel(2, mode=pyb.Timer.ENC_AB, pin=self.pinB7)
        
        # setting time vairables to be used in loop
        self.lastTime = utime.ticks_ms()
        self.period = 750000 #492000
        self.nextTime = utime.ticks_add(self.lastTime, self.period)
        self.thisCount_old= self.timer.counter() 

    def update(self):
    
        self.thisTime = utime.ticks_us()
                
                
        if utime.ticks_diff(self.thisTime, self.nextTime) >= 0:
            
            # Timestamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            
            self.nextCount = self.timer.counter()  
            
            self.deltaCount = self.nextCount - self.thisCount_old 
            
            if self.deltaCount < int(-self.period/2): 
                
                # fixing bad delta reading for large negative deltas
                self.fixedDelta= self.deltaCount + self.period
                self.thisCount_new = self.thisCount_old + self.fixedDelta
                self.angle = self.thisCount_new*360/4000
                self.thisCount_old = self.thisCount_new
                
                return self.angle
            
            elif int(-self.period/2) < self.deltaCount < int(self.period/2):
                
                # good readings
                self.thisCount_new = self.thisCount_old + self.deltaCount 
                self.angle = self.thisCount_new*360/4000
                self.thisCount_old = self.thisCount_new
                
                return self.angle
                
            elif self.deltaCount > int(self.period/2):
              
                # fixing bad readings
                self.fixedDelta = self.deltaCount - self.period
                self.thisCount_new = self.thisCount_old + self.fixedDelta
                self.angle = self.thisCount_new*360/4000
                self.thisCount_old = self.thisCount_new
                return self.angle
            
    def zero_position(self):
        self.timer.counter(0)
        message = str('Encoder has been set to 0')
        return message
                        
    def get_position(self):
        
        self.currentCount = self.timer.counter()
        self.angle = self.currentCount*360/4000
        return self.angle
        
    def get_delta(self):
        self.thisTime = utime.ticks_us()
                
        if utime.ticks_diff(self.thisTime, self.nextTime) >= 0:
            
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            
            self.nextCount = self.timer.counter()  
            
            self.deltaCount = self.nextCount - self.thisCount_old 

            return self.deltaCount

   