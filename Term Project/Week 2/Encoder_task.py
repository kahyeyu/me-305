'''@file Encoder_task.py
@brief This code is responsible for running the encoder driver and its respective functions. 
@author Kahye Yu
@date 4/4/21
@detail The source code is found here on my bitbucket account:
    This code is what main.py runs and will output numbers based on a valid input is entered by the user.
'''


from pyb import USB_VCP
from Encoder_Driver import Encoder_Driver

myusbvcp = USB_VCP()

class Encoder_task:

    def __init__(self, g=True):
        self.g = g
        self.state = 0
        
    def run(self):
        
        self.driver_task = Encoder_Driver(True)
            
        self.update = self.driver_task.update()
        
        self.zero_position = self.driver_task.zero_position()
        self.get_position = self.driver_task.get_position()
        self.get_delta = self.driver_task.get_delta()
        
           
                
                
                
                
                
                
                
                
                
                
                
                
                
                
        # elif self.state == 1:
        #     self.values[self.n] = exp(-self.times[self.n]/10)*sin(2*pi/3*self.times[self.n])
        #     self.n += 1
            
        #     if self.n >= len(self.times):
        #         self.n = 0
        #         self.state = 2
                    
        
        # elif self.state == 2: # Data sending state
        #     self.data_lines = '{:}, {:}\r\n'.format(self.times[self.n], self.values[self.n])
        #     self.n += 1
        #     myusbvcp.write(self.data_lines.encode())

                    
        #     if self.n >= len(self.times):
        #         pass