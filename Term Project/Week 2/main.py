'''@file main.py
@brief This code is run by default and automatically runs the encoder task and the UI task
@author Kahye Yu
@date 4/4/21
@detail The source code is found on my bitbucket account:
'''


from pyb import USB_VCP
from UI_task import UI_task
from Encoder_task import Encoder_task

myusbvcp = USB_VCP()

if __name__=="__main__":
    task1 = Encoder_task(True)
    task2 = UI_task.run(True)
    
    while True:
        task1.run()
        task2.run()
    
    
