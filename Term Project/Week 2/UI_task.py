'''@file UI_task.py
@brief This code is responsible for communication data for serial in both directions. It recieves the user input, collects the desired data and sends it back to the frontend.
@author Kahye Yu
@date 4/4/21
@detail The source code is found on my bitbucket account
The image for the FSM used is shown below
@image html FF_2_FSM.jpg "Finite State Machine for UI task Week 2" width=600cm

'''
from pyb import USB_VCP
from shares import shares

myusbvcp = USB_VCP()

class UI_data_gen:

    def __init__(self, g=True):
        '''@brief This part initializes the class such that will eventually be replaced by some input as a part of main.py
        
        '''
        self.g = g
        self.state = 0
        self.n = 0
        
    def run(self):
        self.shares = shares
        
        if self.state == 0:
                
            if myusbvcp.any():
                self.val = myusbvcp.readline().decode() 
            
                if self.val == 'z':
                    self.output = shares.zero_position
                    myusbvcp.write(self.output.encode())
                    self.state = 1
                
                if self.val == 'p':
                    self.output = shares.get_position
                    myusbvcp.write(self.output.encode())
                    self.state = 2
                
                elif self.val == 'd':
                    self.output = shares.get_delta
                    myusbvcp.write(self.output.encode())
                    self.state = 3
                    
                    
                
            
                   
                    
                    
                        
                        
                        
                        
                        
                        
                        
                        
                
        # elif self.state == 1:
        #     self.values[self.n] = exp(-self.times[self.n]/10)*sin(2*pi/3*self.times[self.n])
        #     self.n += 1
            
        #     if self.n >= len(self.times):
        #         self.n = 0
        #         self.state = 2
                    
        
        # elif self.state == 2: # Data sending state
        #     self.data_lines = '{:}, {:}\r\n'.format(self.times[self.n], self.values[self.n])
        #     self.n += 1
        #     myusbvcp.write(self.data_lines.encode())

                    
        #     if self.n >= len(self.times):
        #         pass
                
                    
               
            
            
            
            
            
            # self.i = 0
            # for self.i in range(len(self.data_lines)):
            #     self.send_encoded_line_of_data = self.data_lines[self.i].encode
            #     self.i += 1
                


        


