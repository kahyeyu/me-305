''' @file Lab_2.py
    @brief This code implements a finite state machine such that the press of the button will cycle through 3 different LED patterns continuously until the user stops the program.
    @author Kahye Yu
    @date 3/12/21
    @detail The source code is found on my bitbucket account
    The image for the FSM is linked below.
    @image html Lab_2_FSM.jpg
    In addition, the video to demonstrate the LED pattern cycle is found here: https://youtu.be/ANSbSPDQ2Es

'''

import utime
import time
import pyb
import math 
from pyb import USB_VCP
USB_VCP 

pinC13 = pyb.Pin(pyb.Pin.cpu.C13)  
start_time = 0
end_time = 0
a = 0
s = 0

def ButtonPress(IRQ_src):
    '''@brief This functions takes the input from the button and triggers the changing state for the FSM.
    @detail A time stamp is triggered at the falling edge of the button push. Another time stamp is taken during the rising edge of the button push. 
    The time between the two time stamps is the time the button was held. If the button was held for less than 100ms, then it prompts the user to try again. If the button 
    was held for more than 800ms, it prompts the user to try again. Otherwise, if the button is pushed for the right amount of time, then it will go to the next state. The 
    code makes sure that s does not go beyond the third state machine.
    @param IRQ_src is the input parameter. It is the trigger from the button press. It is currently set to register both a rising edge and falling edge. 
    @return If pushed correctly, the return value is a value of s that is increased by 1. 
    
    '''
    global start_time
    global end_time
    global a
    a += 1

    if a == 1:
        start_time = utime.ticks_ms()
    elif a == 2:
        end_time = utime.ticks_ms()
        a = 0
        time_held = end_time-start_time
    
        if time_held < 100:
            print('The button was pushed too fast, try again')
        elif time_held > 800:
            print('The button was pushed too slow, try again')
        else:
            global s
            s += 1
            if s == 4:
                s=1
            
ButtonOn = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING, pull=pyb.Pin.PULL_NONE, callback=ButtonPress)  


        
def Square():
    '''@brief This function is the squarewave pattern used by the nucleo to power the LED.
    @detail It runs a continous loop for a high and low state, mimicking the on and off of the LED.
    @param It is only triggered if the state machine is at state 1.
    @return The squarewave pattern is shown on the LED.
    
    '''
    print('Squarewave Pattern Selected')
    a = 0
    while s == 1:
        try:
            if a == 0:
                pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
                pinA5.high()
                time.sleep(1)
                a=1
                
            elif a == 1: 
                pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
                pinA5.low()
                time.sleep(1)
                a=0
            else:
                pass
        except KeyboardInterrupt:
            break       
        
def Sinewave():
    '''@brief This function is responsible for producing the sinewave LED pattern seen on the nucleo.
    @detail It uses the time module, modulus operator and a little bit of math to produce a sinewave. This sinewave is used as an input for PWM such that the LED 
    brightness follows the sinewave pattern.
    @param It is only triggered if the state machine is at state 2.
    @return The sinewave pattern is shown on the LED.
    '''
    print('Sinewave Pattern Selected')
    while s == 2:
        try:
             t = ((time.time() % 10)/10)*2*3.14
             wave = ((math.sin(t))/2)*100 + 50
             pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
             tim2 = pyb.Timer(2, freq = 20000)
             t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
             t2ch1.pulse_width_percent(wave)
                
        except KeyboardInterrupt:
            break
                
def Sawtooth():
    '''@brief This function is responsible for producing the sawtooth LED pattern seen on the nucleo.
    @detail It uses the time module, modulus operator and a little bit of math to produce a sawtooth wave. This sawtooth is used as an input for PWM such that the LED 
    brightness follows the sawtooth pattern.
    @param It is only triggered if the state machine is at state 3.
    @return The sawtooth pattern is shown on the LED.
    
    '''
    print('Sawtooth Pattern Selected')    
    while s == 3:
        try:
            tick = utime.ticks_ms()/1000
            t = 100*(tick % 1)
            pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
            tim2 = pyb.Timer(2, freq = 20000)
            t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
            t2ch1.pulse_width_percent(t)
            
        except KeyboardInterrupt:
            break   
        
if __name__=='__main__':
    print('Welcome!\nPlease press the blue button to cycle through the LED patterns\nOnce you would like to finish, press Ctrl+C')
    s=0
    while True:
        try:
            if s==0:
                pass
                    
            elif s==1: 
                Square()
        
            elif s==2:
                Sinewave()
                
            elif s==3: 
                Sawtooth()
           
        except KeyboardInterrupt:
            break
                     
       